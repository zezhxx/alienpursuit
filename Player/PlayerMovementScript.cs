using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementScript : MonoBehaviour
{
    public float WalkSpeed, RotationSpeed;
    public bool TimeOver;
    public Vector3 Speed, Rotation;
    public Animator PlayerAnimator, NetAnimator, NetColliderAnimator;
    public GameObject NetCollider;

    private Rigidbody PlayerRig;
    private Vector2 InputVector;
    // Start is called before the first frame update
    void Start()
    {
        PlayerRig = GetComponent<Rigidbody>();
        TimeOver = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        //Debug.Log(InputVector.sqrMagnitude);
        if (TimeOver == false)
        {
            if (InputVector.sqrMagnitude != 0)
            {
                //Myanim.SetBool("EstoyAndandoAdelante", false);
                PlayerAnimator.SetBool("Moving", true);
                NetAnimator.SetBool("Moving", true);
            }
            else
            {
                PlayerAnimator.SetBool("Moving", false);
                NetAnimator.SetBool("Moving", false);
            }

            Speed = new Vector3(InputVector.x * WalkSpeed * Time.deltaTime, PlayerRig.velocity.y, InputVector.y * WalkSpeed * Time.deltaTime);
            Rotation = new Vector3(InputVector.x * RotationSpeed, 0, InputVector.y * RotationSpeed);
            PlayerRig.velocity = Speed;

            if (InputVector.sqrMagnitude == 0) return;

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Rotation), 0.15f);
        }
        if (TimeOver == true)
        {
            PlayerAnimator.SetBool("Moving", false);
            NetAnimator.SetBool("Moving", false);
            PlayerAnimator.SetBool("Attacking", false);
            NetAnimator.SetBool("Attacking", false);
            NetColliderAnimator.SetBool("Attacking", false);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        InputVector = context.ReadValue<Vector2>();
    }

    public void OnCatch(InputAction.CallbackContext context)
    {
        if (TimeOver == false)
        {
            if (context.started)
            {
                PlayerAnimator.SetBool("Attacking", true);
                NetAnimator.SetBool("Attacking", true);
                NetColliderAnimator.SetBool("Attacking", true);
                //NetCollider.SetActive(true);
            }

            if (context.canceled)
            {
                PlayerAnimator.SetBool("Attacking", false);
                NetAnimator.SetBool("Attacking", false);
                NetColliderAnimator.SetBool("Attacking", false);
                //NetCollider.SetActive(false);
            }
        }
            
    }
}
