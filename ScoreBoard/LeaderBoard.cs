using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Dan.Main;


public class LeaderBoard : MonoBehaviour
{
    //Secret Key Leader Board e35547acee7791d89c6a48db27371efad2d2a6ae066bca648cc1be2eb451fb438e8d828424078c82da8596e003baffb7613bd29d002d98ab323e2dd2cf65922962f12adcdcc885aeeea7d34471556cc43bd01073dbf7857926a8e3b8fe7c3b7c7593f6d8c4417e689cebde376d749a7e1b285c6da1a62522328a104b03b6695f
    [SerializeField]
    private List<TextMeshProUGUI> names;
    [SerializeField]
    private List<TextMeshProUGUI> score;

    private string publicLeaderKey =
        "e3e188479d3df9d6d756b1ca108472f98b8b0c644060f91e847965a23640497f";

    private void Start()
    {
        GetLeaderboard();
        
    }
    public void GetLeaderboard()
    {
        LeaderboardCreator.GetLeaderboard(publicLeaderKey, ((msg) =>
        {
            int loopLength = (msg.Length < names.Count) ? msg.Length : names.Count;
            
            for (int i = 0; i < loopLength; ++i)
            {
                names[i].text = msg[i].Username;
                score[i].text = msg[i].Score.ToString();
            }
        }));
    }

    public void SetLeaderboardEntry(string username, int score)
    {
        LeaderboardCreator.UploadNewEntry(publicLeaderKey, username, score, ((msg) =>
            {
                //if (System.Array.IndexOf(badWords, name) != -1) return;
                GetLeaderboard();
            }));

    }
}
