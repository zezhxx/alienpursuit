using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MixerScript : MonoBehaviour
{
    [SerializeField] private AudioMixer Mixer;
    private float MusicVolume, PrefsVolume;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(MusicVolume);
        Mixer.SetFloat("Volume", PrefsVolume);
        PrefsVolume = PlayerPrefs.GetFloat("volume");
        PlayerPrefs.SetFloat("volume", MusicVolume);
    }
    public void CambiarVolumen(float volumen)
    {
        MusicVolume = volumen;
        
    }
}
