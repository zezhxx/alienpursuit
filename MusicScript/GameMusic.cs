using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMusic : MonoBehaviour
{
    public AudioSource Music;
    public bool OnGame;
    // Start is called before the first frame update
    void Start()
    {
        OnGame = true;
        Music.volume = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (OnGame == true)
        {
            Music.volume += 0.005f;
        }
        if (OnGame == false)
        {
            Music.volume -= 0.005f;
        }
    }
    public void OnGameFalse()
    {
        OnGame = false;
    }
}
