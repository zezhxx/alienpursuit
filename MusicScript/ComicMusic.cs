using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComicMusic : MonoBehaviour
{
    public AudioSource Music;
    public bool OnComic;
    // Start is called before the first frame update
    void Start()
    {
        OnComic = true;
        Music.volume = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (OnComic == true && Music.volume < 0.75f)
        {
            Music.volume += 0.005f;
        }
        if (OnComic == false)
        {
            Music.volume -= 0.005f;
        }
    }
    public void OnComicFalse()
    {
        OnComic = false;
    }
}
