using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ComicAndGameMixer : MonoBehaviour
{
    [SerializeField] private AudioMixer Mixer;
    private float MusicVolume, PrefsVolume;
    // Start is called before the first frame update
    void Start()
    {
        PrefsVolume = PlayerPrefs.GetFloat("volume");
    }

    // Update is called once per frame
    void Update()
    {
        Mixer.SetFloat("Volume", PrefsVolume);
    }
}
