using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour
{

    public AudioSource Music;
    public bool OnMenu;

    // Start is called before the first frame update
    void Start()
    {
        OnMenu = true;
        Music.volume = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (OnMenu == true)
        {
            Music.volume += 0.005f;
        }
        if (OnMenu == false)
        {
            Music.volume -= 0.005f;
        }
    }
    public void OnMenuFalse()
    {
        OnMenu = false;
    }
}
