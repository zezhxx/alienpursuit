using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AlienScript : MonoBehaviour
{
    public AudioSource CatchAudio;
    private GameObject UI;
    private CanvasScript UIScrip;
    private Animator AlienAnimator;
    public Transform target;
    public float Speed;

    void Start()
    {
        UI = GameObject.Find("UIManager");
        UIScrip = UI.GetComponent<CanvasScript>();
        AlienAnimator = gameObject.GetComponent<Animator>();
        target = GameObject.FindWithTag("Player").transform;
        Speed = 2;
        AlienAnimator.Play("Apearing");
    }

    void Update()
    {
        
        Vector3 direction = transform.position - target.position;
        AlienAnimator.SetBool("Moving", false);


        if (direction.sqrMagnitude < 30f)
    {
        transform.Translate(direction.normalized * Time.deltaTime * Speed, Space.World);
        transform.forward = direction.normalized;
        AlienAnimator.SetBool("Moving", true);

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Net")
        {
            UIScrip.Points = UIScrip.Points + 5;
            StartCoroutine(CatchedCoroutine());
            
        }

        if (other.gameObject.tag == "Crater")
        {            
            Destroy(this.gameObject);
        }
    }

    IEnumerator CatchedCoroutine()
    {
        CatchAudio.enabled = true;
        this.gameObject.GetComponent<Collider>().enabled = false;
        AlienAnimator.Play("Disapearing");
        
        yield return new WaitForSeconds(1);

        Destroy(this.gameObject);
    }
}
