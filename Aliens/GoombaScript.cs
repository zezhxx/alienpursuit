using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GoombaScript : MonoBehaviour
{
    public AudioSource CatchAudio;
    private GameObject UI;
    private CanvasScript UIScrip;
    private Animator GoombaAnimator;
    public Transform target;
    public float Speed;

    // Start is called before the first frame update
    void Start()
    {
        UI = GameObject.Find("UIManager");
        UIScrip = UI.GetComponent<CanvasScript>();
        GoombaAnimator = gameObject.GetComponent<Animator>();
        target = GameObject.FindWithTag("Player").transform;
        Speed = 1.5f;
        GoombaAnimator.Play("Apearing");
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 direction = transform.position - target.position;
        GoombaAnimator.SetBool("Moving", false);


        if (direction.sqrMagnitude < 30f)
        {
            transform.Translate(direction.normalized * Time.deltaTime * Speed, Space.World);
            transform.forward = direction.normalized;
            GoombaAnimator.SetBool("Moving", true);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("AAAA");

        if (other.gameObject.tag == "Net")
        {
            //Debug.Log("Net");
            UIScrip.Points= UIScrip.Points + 3;

            StartCoroutine(CatchedCoroutine());
            
        }

        if (other.gameObject.tag == "Crater")
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator CatchedCoroutine()
    {
        CatchAudio.enabled = true;
        this.gameObject.GetComponent<Collider>().enabled = false;
        GoombaAnimator.Play("Disapearing");
        yield return new WaitForSeconds(1);

        Destroy(this.gameObject);
    }
}
