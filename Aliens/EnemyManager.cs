using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{

    public GameObject Enemy1, Enemy2;
    public float SpawnAreaNegativeX, SpawnAreaPositiveX, SpawnAreaNegativeZ, SpawnAreaPositiveZ;
    public float timer;
    public bool CanEnemySpawn;
    private int EnemyNumber;

    void Start()
    {
        CanEnemySpawn = true;
    }

    void Update()
    {
        if (CanEnemySpawn == true)
        {
            timer -= Time.deltaTime;
            if (timer < 0f)
            {
                SpawnEnemy();
                timer = Random.Range(4, 15);
            }
        }
        
    }

    private void SpawnEnemy()
    {
        Vector3 position = new Vector3(
            UnityEngine.Random.Range(SpawnAreaNegativeX, SpawnAreaPositiveX),
            0f,
            UnityEngine.Random.Range(SpawnAreaNegativeZ, SpawnAreaPositiveZ)
            );
        EnemyNumber = Random.Range(1, 3);
        if (EnemyNumber == 1)
        {
            GameObject NewEnemy1 = Instantiate(Enemy1);
            NewEnemy1.transform.position = position;
        }
        else
        {
            GameObject NewEnemy2 = Instantiate(Enemy2);
            NewEnemy2.transform.position = position;
        }
    }
}
