using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class CanvasScript : MonoBehaviour
{
    public GameObject EnemySpawnZone1, EnemySpawnZone2, EnemySpawnZone3, EnemySpawnZone4, EnemySpawnZone5, EnemySpawnZone6;
    public GameObject EnemySpawnZone7, EnemySpawnZone8, EnemySpawnZone9, EnemySpawnZone10, EnemySpawnZone11, Player, TimeOver;

    public TMP_Text TimerDisplay;
    private float Timer;
    public TMP_Text PointsDisplay;
    private int PointsGained;
    public int Points
    {
        get
        {
            
            return PointsGained;
        }
        set
        {
            
            PointsGained = value;
            PointsDisplay.text = PointsGained.ToString();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Timer = 60f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Timer > 0)
        {
            Timer -= Time.deltaTime;
        }
        else
        {
            //finish
            EnemySpawnZone1.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone2.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone3.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone4.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone5.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone6.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone7.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone8.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone9.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone10.GetComponent<EnemyManager>().CanEnemySpawn = false;
            EnemySpawnZone11.GetComponent<EnemyManager>().CanEnemySpawn = false;
            Player.GetComponent<PlayerMovementScript>().TimeOver = true;
            TimeOver.SetActive(true);
        }
        TimerDisplay.text = "Time: " + Timer.ToString("f0");
    }
}
