using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    
    // Start is called before the first frame update
    public void EnterGame()
    {
        SceneManager.LoadScene("AlienPursuit");
    }

    public void EnterComic()
    {
        SceneManager.LoadScene("ComicScene");
    }

    public void EnterMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
