using UnityEngine;
using Dan.Main;


public class UIManager : MonoBehaviour
{
    public GameObject FadeIn, FadeOutExit, FadeInMenu, Music;
    public void StartGame()
    {
        //SceneManager.LoadScene("AlienPursuit");
        FadeIn.SetActive(true);
        Music.gameObject.GetComponent<MenuMusic>().OnMenuFalse();
    }

    public void ReturnToMenu()
    {
        //SceneManager.LoadScene("Menu");
        LeaderboardCreator.ResetPlayer();
        FadeInMenu.SetActive(true);
        Music.gameObject.GetComponent<GameMusic>().OnGameFalse();
    }


    public void ExitGame()
    {
        FadeOutExit.SetActive(true);
    }
}
