using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager2 : MonoBehaviour
{
    public GameObject Menu;
    public GameObject Volumen;
    public GameObject Scoreboard;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IrAlMenu()
    {
        Menu.SetActive(true);
        Volumen.SetActive(false);
        Scoreboard.SetActive(false);


    }
    public void IrAVolumen()
    {
        Menu.SetActive(false);
        Volumen.SetActive(true);
        Scoreboard.SetActive(false);


    }

    public void IrAScoreboard()
    {
        Menu.SetActive(false);
        Volumen.SetActive(false);
        Scoreboard.SetActive(true);


    }
}